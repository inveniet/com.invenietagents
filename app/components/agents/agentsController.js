/* TEST 2*/

angular.module('invenietAgents').controller(

    /**
     * aboutController name
     */
    'agentsController',

    [
        /**
         * Include any dependencies
         *
         * @link https://docs.angularjs.org/guide/module#dependencies
         */
        '$scope',
        'formpopulateService',

        /**
         * This Anonymous function will be called when the relevant view is rendered.
         * $scope is a variable available within that view and can be accessed with
         * braces.
         *
         * @param object           $scope
         *
         */
        function($scope, formService) {

            /**
             * Set the user data.
             */
            $scope.applicant = formService.getApplicantInfo();

        }
    ]
);
