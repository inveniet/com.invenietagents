/* TEST 2*/

angular.module('invenietAgents').controller(

     /**
      * aboutController name
      */
     'homeController',

     [
          /**
           * Include any dependencies
           *
           * @link https://docs.angularjs.org/guide/module#dependencies
           */
          '$scope',
          'titleService',
          /**
           * This Anonymous function will be called when the relevant view is rendered.
           * $scope is a variable available within that view and can be accessed with
           * braces.
           *
           * @param object           $scope
           *
           */
          function($scope, ts) {
               ts.setTitle('');
               $scope.header = "About Us";
               $scope.message = "You Are In About!";
          }
     ]
);
