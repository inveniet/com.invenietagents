/* TEST 2*/

angular.module('invenietAgents').controller(

    /**
     * aboutController name
     */
    'searchingController',

    [
        /**
         * Include any dependencies
         *
         * @link https://docs.angularjs.org/guide/module#dependencies
         */
        '$scope',
        'formpopulateService',
        '$window',

        /**
         * This Anonymous function will be called when the relevant view is rendered.
         * $scope is a variable available within that view and can be accessed with
         * braces.
         *
         * @param object           $scope
         *
         */
        function($scope, formService, $window) {

          $scope.formKey = formService.formValidationKey;
          $scope.alreadyApplied = false;
          $scope.validationResults = formService.validateEntireForm();
          $scope.applicant = formService.getApplicantInfo();

          if($scope.applicant.formComplete === true){
            $scope.alreadyApplied = true;
          }else{

          if($scope.validationResults.formValid){
              formService.processForm(function (response){
                if(response == 200){
                  $scope.applicant.formComplete = true;
                  formService.setApplicantInfo($scope.applicant);
                  setTimeout(function (){
                    $window.location.href = '/jose';
                  }, 10000);
                }
                if(response == 100){
                  $validationResults.formValid = false;
                }
              });
          }
          }


        }
    ]
);
