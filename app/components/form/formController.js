/* TEST 2*/

angular.module('invenietAgents').controller(

    /**
     * aboutController name
     */
    'formController',

    [
        /**
         * Include any dependencies
         *
         * @link https://docs.angularjs.org/guide/module#dependencies
         */
        '$scope',
        'titleService',
        'formpopulateService',
        '$route',
        '$location',
        'zipWhiteList',

        /**
         * This Anonymous function will be called when the relevant view is rendered.
         * $scope is a variable available within that view and can be accessed with
         * braces.
         *
         * @param object           $scope
         *
         */
        function($scope, ts, formService, route, location, Zity) {
            /**
             * Define form.
             */
            var $form = $(".inveniet-agents-form");

            /**
             * Set next and back variable. These should be set in markup.
             */
            $scope.next = undefined;
            $scope.back = undefined;

            /**
             * Set the patterns
             */
            $scope.patternsPhone = /^(\+\d{1,2}\s)?\(?\d{3}\)?[\s.-]\d{3}[\s.-]\d{4}$/;
            $scope.patternsZip = /^\d{5}(?:[-\s]\d{4})?$/;

            $scope.whitelistedCities = [
                // "ATWATER",
                "CERES",
                "CHOWCHILLA",
                "CLOVIS",
                "DELHI",
                "DOS PALOS",
                "FIREBAUGH",
                "FRESNO",
                "GUSTINE",
                "LIVINGSTON",
                "LOS BANOS",
                "MADERA",
                "MENDOTA",
                "MERCED",
                "MODESTO",
                "NEWMAN",
                "TURLOCK"
            ]


            $scope.changeValidity = function(result) {
              if(result){
                $scope.userForm.zip.$setValidity("cityNotSupported", true);
              }else{
                $scope.userForm.zip.$setValidity("cityNotSupported", false);
              }
              $scope.$apply();
            }

            $scope.checkZip = function(){
              Zity.checkZip($scope.applicant.zip, $scope.whitelistedCities, function(result){
                $scope.changeValidity(result);
              })
            };

            /**
             * Set the user data.
             */
            $scope.applicant = formService.getApplicantInfo();
            console.dir($scope.applicant);

            /**
             * Handles next step
             */
            $scope.submitForm = function() {
                // event.preventDefault();
                var has_class = $(this).hasClass('disabled');
                if ($scope.userForm.$valid) {
                    $(".view-animate-container").removeClass("back");

                    formService.setApplicantInfo($scope.applicant);

                    $(".checkmarkWrapper").show().addClass('animated');
                    location.path("/" + $scope.next);

                    setTimeout(function() {
                        $(".checkmarkWrapper").fadeOut("fast", function() {
                            $(this).removeClass('animated');
                        });
                    }, 1500)
                } else {

                }

            }

            /**
             * Handle going backwords
             */
            $scope.previousStep = function() {
                $(".view-animate-container").addClass("back");
                location.path("/" + $scope.back);
            }

        }
    ]
);
