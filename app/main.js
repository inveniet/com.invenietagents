// Declare  app
(function() {
    var invenietAgents = angular.module('invenietAgents', ['ngRoute', 'ngAnimate', 'zj.namedRoutes', 'ngCookies', 'ui.mask']);

    /**
     * Encender Routes
     *
     * Configure routes for Encender Website
     */

    invenietAgents.config(["$routeProvider", "$locationProvider", function($routeProvider, $locationProvider) {

        $routeProvider

        // Home route
            .when('/', {
                name: 'home',
                templateUrl: "/app/components/home/homeView.html",
                controller: "homeController"
            })
            .when('/step1', {
                name: 'Step1',
                templateUrl: "/app/components/home/homeView.html",
                controller: "homeController"
            })

        .when('/step2', {
                name: 'Step2',
                templateUrl: "/app/components/form/step2/step2View.html",
                controller: "formController"
            })
            .when('/step3', {
                name: 'Step3',
                templateUrl: "/app/components/form/step3/step3View.html",
                controller: "formController"
            })
            .when('/step4', {
                name: 'Step4',
                templateUrl: "/app/components/form/step4/step4View.html",
                controller: "formController"
            })
            .when('/step5', {
                name: 'Step5',
                templateUrl: "/app/components/form/step5/step5View.html",
                controller: "formController"
            })
            .when('/step6', {
                name: 'Step6',
                templateUrl: "/app/components/form/step6/step6View.html",
                controller: "formController"
            })
            .when('/step7', {
                name: 'Step7',
                templateUrl: "/app/components/form/step7/step7View.html",
                controller: "formController"
            })
            .when('/searching', {
                name: 'searching',
                templateUrl: "/app/components/form/searching/searchingView.html",
                controller: "searchingController"
            })
            .when('/declined', {
                name: 'declined',
                templateUrl: "/app/components/form/declined/declinedView.html"
            })
            .when('/jose', {
                name: 'jose',
                templateUrl: "/app/components/agents/jose/joseView.html",
                controller: "agentsController"

            })

        // Handle 404
        .otherwise({redirectTo : '/'});

        // Use the HTML5 History API
        $locationProvider.html5Mode(true);
    }]);

    invenietAgents.run(['$anchorScroll', '$window', function($anchorScroll, $window) {
        // hack to scroll to top when navigating to new URLS but not back/forward
        var wrap = function(method) {
            var orig = $window.window.history[method];
            $window.window.history[method] = function() {
                var retval = orig.apply(this, Array.prototype.slice.call(arguments));
                $anchorScroll();
                return retval;
            };
        };
        wrap('pushState');
        wrap('replaceState');
    }])
})();
