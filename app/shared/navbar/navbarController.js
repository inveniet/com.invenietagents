/**
 |------------------------------
 | navbarController
 |------------------------------
 | All code that will be used when a navbar is rendered.
 |
 |
 */


/**
 * navbar controller is to be used for the name navbar on the top of all pages.
 */
angular.module('invenietAgents').controller(

     /**
      * navbarController name
      */
     'navbarController',

     [
          /**
           * Include any dependencies
           *
           * @link https://docs.angularjs.org/guide/module#dependencies
           */
          '$scope',
          '$location',

          /**
           * This Anonymous function will be called when the relevant view is rendered.
           * $scope is a variable available within that view and can be accessed with
           * braces.
           *
           * @param object           $scope
           *
           */
          function($scope, $location) {
               $scope.isActive = function (viewLocation){
                 return viewLocation === $location.path();
               }
          }
     ]
);
