/**
 |---------------------------------
 | Title
 |---------------------------------
 |
 | Description
 |
 | @author Felipe Tadeo
 */

angular
    .module('invenietAgents')
    .directive('ageLimit', [

        function() {

          publicMethods = {
            restrict : 'A',
            require  : 'ngModel',

            // Returns the age
            ageCheck : function(birthDateString) {

                    var today = new Date();
                    var birthDate = new Date(birthDateString);
                    var age = today.getFullYear() - birthDate.getFullYear();
                    var m = today.getMonth() - birthDate.getMonth();
                    if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
                        age--;
                    }
                    return age;
                },

                convertToDateString : function(str){
                  var
                    mm   = str.substring(0,2),
                    dd   = str.substring(2,4),
                    yyyy = str.substring(4,8);

                    return mm + "/" + dd + "/" + yyyy;
                },

                link: function (scope, elm, attrs, ctrl){
                  console.log("checking age limit...");
                  ctrl.$parsers.unshift(function(value){
                    var
                      dateString = publicMethods.convertToDateString(value),
                      age = publicMethods.ageCheck(dateString);

                    console.log("Checking age limit for " + dateString);
                    console.log("Customer is: " + age);
                    console.log("Age restriction is:" + attrs.minAge);

                    // Check it now
                    ctrl.$setValidity('ageTooLow', (age >= attrs.minAge));

                    return value;
                  });
                }



              }
            return publicMethods;
          }

    ])
