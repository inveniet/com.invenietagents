/**
 |---------------------------------
 | Title
 |---------------------------------
 |
 | Description
 |
 | @author Felipe Tadeo
 */

 angular
  .module('invenietAgents')
  .directive('autoFillSync', [
    "$timeout",

    function($timeout){
      loopIt = function(elem, ngModel, origVal){
          var newVal = elem.val();
          if(origVal !== newVal){
            ngModel.$setViewValue(newVal);
            console.log("timeout ended");
          }else{
            setTimeout(function (){
              loopIt(elem, ngModel, origVal);
            }, 500);
          }


      }
      return {
        require  : 'ngModel',
        restrict : 'A',

        link : function (scoep, elem, attrs, ngModel){
          var origVal = elem.val();
          loopIt(elem, ngModel, origVal);

        }
      }
    }
  ]);
