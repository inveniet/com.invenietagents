/**
 |---------------------------------
 | jqueryUiDatePicker
 |---------------------------------
 |
 | Use this datepicker directive to assign the datepicker
 | UI widget to an element. This datepicker will restrict
 | the user to any day but Saturdays and any day past today.
 |
 | Example:
 |
 | <input type="text" jquery-ui-datepicker />
 |
 | @author Felipe Tadeo
 */

angular
    .module('invenietAgents')
    .directive('jqueryUiDatepicker', [
      /**
       * $parse
       * Converts Angular expression into a function.
       *
       * @link https://docs.angularjs.org/api/ng/service/$parse
       */
      '$parse',

        function($parse) {

            publicMethods = {

                /**
                 * DOM Element must have an ng-model attribute.
                 */
                require: 'ngModel',

                /**
                 * jquery-ui-datepicker must be strictly used as an attribute.
                 */
                restrict: 'A',

                /**
                 * @function link
                 *
                 * Assign the datepicker ui widget to the element.
                 */
                link: function(scope, elm, attrs, ctrl) {
                    // Get the model
                    parsed = $parse(attrs.ngModel);

                    $(elm)
                        // Assign datepicker
                        .datepicker({
                            /**
                             * Must be greater than today
                             */
                            minDate: 1,
                            /**
                             * Must not be greater than a month from now.
                             */
                            maxDate: "+1M",
                            /**
                             * Must be on any day but Saturday
                             */
                            beforeShowDay: function(day) {
                                var day = day.getDay();
                                if (day == 6) {
                                    return [false, 'class'];
                                } else {
                                    return [true, 'class'];
                                }
                            },
                            /**
                             * Update scope for validation when user changes date.
                             */
                            onSelect : function(dateText, inst){
                              scope.$apply(function(){
                                parsed.assign(scope, dateText)
                              })
                            }
                        })
                        /**
                         * Do not allow user to edit this manualy.
                         */
                        .attr('readonly', true);

                }

            }
            return publicMethods;
        }

    ])
