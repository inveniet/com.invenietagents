/**
 |---------------------------------
 | Title
 |---------------------------------
 |
 | Description
 |
 | @author Felipe Tadeo
 */

angular
    .module('invenietAgents')
    .service('zipWhiteList', [

        function() {
            var
            /**
             *
             */
                uid = "188INVEN3710",

                /**
                 *
                 */
                api = "CityStateLookup",

                /**
                 *
                 */
                url = "http://production.shippingapis.com/ShippingAPITest.dll",

                /**
                 *
                 */
                publicMethods = {
                    // Directive settings


                    /**
                     * @function getCity
                     *
                     * Returns the name of the city associated with the @param zip.
                     * then calls the @param callback.
                     *
                     * @param zip string. The 9 digit zip code
                     */
                     getCity: function(zip, callback) {
                        var xml = '<CityStateLookupRequest USERID="' + uid + '"><ZipCode ID= "0"> <Zip5>' + zip + '</Zip5></ZipCode></CityStateLookupRequest>';
                        $.ajax({
                                // type: 'xml',
                                url: url,
                                data: {
                                    api: api,
                                    xml: xml
                                }
                            })
                            .done(function(response) {

                                callback($(response).find('City').html());

                            })
                            .fail(function() {
                                callback(false);
                            });
                    },

                    /**
                     * @function link
                     *
                     * Not entirely sure what this does...
                     */
                    checkZip: function(value, list, callback) {
                      // Calling zip
                        var
                          /**
                           * @var zip, the zip code we're checking
                           */
                            zip = value,

                            /**
                             *
                             */
                            cityList = list;

                            publicMethods.getCity(zip, function(city){
                              // Check if city is in array
                              var arrayIndex = $.inArray(city, cityList);

                              callback(arrayIndex > -1);

                            });

                    }



                }
            return publicMethods;
        }

    ])
