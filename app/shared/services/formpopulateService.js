/**
 |---------------------------------
 | Title
 |---------------------------------
 |
 | Description
 |
 | @author Felipe Tadeo
 */

angular
    .module('invenietAgents')
    .service('formpopulateService', [

        "$cookies",
        "settingsService",

        function(cookie, settings) {

            /**
             *
             */
            var applicantInfo = {};

            /**
             *
             */
             var settings = settings.getSettings();

            /**
             * getApplicantInfo
             *
             * Gets the applicant info and initiates it if it's not alread
             * set.
             */
            var publicMethods = {
                getApplicantInfo: function() {
                    var cookieInfo = cookie.getObject('applicant');
                    if (typeof cookieInfo != "object") {
                        return applicantInfo;
                    } else {
                        applicantInfo = cookieInfo;
                        return applicantInfo;
                    }
                },

                /**
                 * setApplicantInfo
                 *
                 * sets applicant info and updates it.
                 */
                setApplicantInfo: function(newInfo) {

                    applicantInfo = $.extend(applicantInfo, newInfo);

                    // set the cookie:
                    cookie.putObject('applicant', applicantInfo);

                },

                /**
                 * processForm
                 *
                 * Will take all values from a form and store them in a cookie.
                 *
                 */
                processForm: function(callback) {
                    // Get cookie
                    console.log("processing form..");
                    var applicationInfo = publicMethods.getApplicantInfo();
                    console.dir(applicationInfo);
                    // deliver post


                      $.ajax({
                          type: "GET",
                          url: settings.API_URL,
                          data: applicationInfo,
                          dataType: 'json',
                          CrossDomain:true
                      })
                      .done(function (data){
                        console.log("API response:");
                        console.log(data);
                        var response = data.response
                        callback(response);
                      });



                },

                validateEntireForm: function() {
                    var applicationInfo = publicMethods.getApplicantInfo();
                    var elements = {
                        formValid: true,
                        invalidSteps: []
                    };
                    $.each(publicMethods.formValidationKey, function(key, value) {
                        // console.log("Checking " + value.name);
                        var stepValid = true;
                        var invalidElements = 0;
                        $.each(value.values, function(k, v) {
                            // console.log("Vlidating: " + k + " = " + applicationInfo[k]);
                            elements[k] = {
                                valid: false
                            };
                            // Check if value is set:
                            if (typeof applicationInfo[k] != 'undefined') {
                                // Test if it checks out in the validation
                                var
                                    tester = applicationInfo[k];
                                tester = tester.toString();
                                if (publicMethods.formValidationExpressions[v.type].test(tester)) {
                                    elements[k] = {
                                        valid: true
                                    };
                                    // console.log('passed regex.');
                                } else {
                                    elements.formValid = false;
                                    stepValid = false;
                                    invalidElements++
                                    // console.log('failed regex.');
                                }
                            } else if (v.required) {
                                elements.formValid = false;
                                stepValid = false;
                                invalidElements++
                                // console.log('is required, but not found.');
                            }
                        });
                        if (!stepValid) {

                            elements.invalidSteps.push({
                                name: value.name,
                                step: key,
                                invalids: invalidElements
                            });
                        }
                    });
                    console.dir(elements);
                    return elements;
                },

                formValidationKey: {
                    'Step1': {
                        name: 'Contact Info',
                        values: {
                            'first_name': {
                                type: 'name',
                                required: true
                            },
                            'last_name': {
                                type: 'name',
                                required: true
                            },
                            'email': {
                                type: 'email',
                                required: true
                            },
                            'phone': {
                                type: 'phone',
                                required: true
                            },
                            'zip': {
                                type: 'zip',
                                required: true
                            },
                        }
                    },
                    'Step2': {
                        name: 'General Info',
                        values: {
                            'dob': {
                                type: 'dob',
                                required: true
                            },
                            'height': {
                                type: 'height',
                                required: true
                            },
                            'gender': {
                                type: 'gender',
                                required: true
                            },
                        }
                    },
                    'Step3': {
                        name: "Tobacco Info",
                        values: {
                            'tobacco': {
                                type: 'boolean',
                                required: true
                            },
                        }
                    },
                    'Step4': {
                        name: "Medical Hystory Part 1",
                        values: {
                            'hypertension': {
                                type: 'boolean',
                                required: true
                            },
                            'hypertensionmedication': {
                                type: 'boolean',
                                required: false
                            },
                            'groupone': {
                                type: 'boolean',
                                required: true
                            },
                            'diabetes': {
                                type: 'boolean',
                                required: true
                            },
                        }
                    },
                    "Step5": {
                        name: "Medical Hystory Part 2",
                        values: {
                            'diseases': {
                                type: 'boolean',
                                required: true
                            },
                            'aidsHiv': {
                                type: 'boolean',
                                required: true
                            },
                            'addiction': {
                                type: 'boolean',
                                required: true
                            },
                        }
                    },
                    "Step6": {
                        name: "General History",
                        values: {
                            'criminalCharges': {
                                type: 'boolean',
                                required: true
                            },
                            'disabilityBenefits': {
                                type: 'boolean',
                                required: true
                            },
                        }
                    },
                    "Step7": {
                        name: "Contact Time",
                        values: {
                            'contactDay': {
                                type: 'day',
                                required: true
                            },
                            'consent': {
                                type: 'boolean',
                                required: true
                            },
                            'contactTime': {
                                type: 'rangeTime',
                                required: true
                            }
                        }
                    }
                },

                formValidationExpressions: {
                    name: /^[a-zA-Z ]+$/,
                    phone: /^\d{10}$/,
                    email: /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i,
                    boolean: /^true|false|0|1$/,
                    zip: /(^\d{5}$)|(^\d{5}-\d{4}$)/,
                    height: /^[3-7][0-1][0-9]$/,
                    gender: /^female|male$/,
                    day: /^\d{2}\/\d{2}\/\d{4}$/,
                    dob: /^([0][0-9]|[1][0-2])([0][1-9]|[1-3][0-9])\d{4}$/,
                    rangeTime: /^([1-9]|[1][0-2])(pm|am|PM|AM)\-([1-9]|[1][0-2])(PM|AM|pm|am)$/

                },

                populateForm: function($form) {
                    publicMethods.getApplicantInfo();

                    var $inputs = $form.find(":input");

                    $.each($inputs, function() {
                        // get the element name
                        var name = $(this).attr("name");
                        if (typeof applicantInfo[name] != 'undefined') {
                            $(this).val(applicantInfo[name]);
                        }
                    });

                }
            }

            return publicMethods;

        }
    ])
