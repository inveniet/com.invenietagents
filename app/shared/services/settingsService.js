/**
 |---------------------------------
 | Title
 |---------------------------------
 |
 | Description
 |
 | @author Felipe Tadeo
 */

 angular
  .module('invenietAgents')
  .service('settingsService', [function(){
    var settings = {
          API_URL : "http://api.invenietagents.com/new-applicant"
      }
    var publicMethods = {
      getSettings : function(){
        return settings;
      }
    }

    return publicMethods;
  }]);
