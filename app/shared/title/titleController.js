/**
 |------------------------------
 | aboutController
 |------------------------------
 | All code that will be used when this view is rendered.
 |
 |
 */

angular.module('invenietAgents').controller(

     /**
      * titleController name
      */
     'titleController',

     [
          /**
           * Include any dependencies
           *
           * @link https://docs.angularjs.org/guide/module#dependencies
           */
          '$scope',
          'titleService',

          /**
           * This Anonymous function will be called when the relevant view is rendered.
           * $scope is a variable available within that view and can be accessed with
           * braces.
           *
           * @param object           $scope
           *
           */
          function($scope, titleService) {
               $scope.titleService = titleService;
          }
     ]
);
