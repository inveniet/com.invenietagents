/*
|--------------------------------------------------------------------------
| titleService File
|--------------------------------------------------------------------------
|
| An Angular service is a singleton object created by a service factory. These
| service factories are functions which, in turn, are created by a service
| provider. The service providers are constructor functions. When instantiated
| they must contain a property called $get, which holds the service factory
| function. 
|
*/

angular
     .module('invenietAgents')
     .service('titleService', function() {

          var title = "Pure Care Solutions";

          return {
               getTitle: function() {
                    return title;
               },

               setTitle: function(newTitle) {
                    var serporator = (newTitle.length > 0)? " - " : " ";
                    title = "Pure Care Solutions" + serporator + newTitle;
               }
          };
     })
