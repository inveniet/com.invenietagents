module.exports = {
  beforeEach: browser => {
    browser
      .url("http://sandbox.invenietagents.com")
      .waitForElementVisible('body', 1000);
  },
  'Check if Step1 loads' : browser =>{
    browser
      .waitForElementVisible('body', 1000)
      .waitForElementVisible('nav.navbar', 1000)
      .waitForElementVisible('.view-animate-container .view-animate .wrapper', 1000)
      .waitForElementVisible('#step1wrapper', 1000)
      .waitForElementVisible('#step1', 1000);
  },
  'Test Terms and Conditions, and Privacy Policy lightboxes.' : browser =>{
    browser
      .waitForElementVisible('#step1wrapper', 2000)
      // Test Terms and conditions
      .click('#openTerms')
      .waitForElementVisible('#termsModal', 1000)
      .assert.visible('#termsModal')
      .waitForElementVisible('#closeTerms', 1000)
      .pause(2000)
      .click('#closeTerms')
      .waitForElementNotVisible('#termsModal', 3000)
      .assert.hidden('#termsModal')
      // Test Privacy Policy
      .click('#openPP')
      .waitForElementVisible('#ppModal', 1000)
      .assert.visible('#ppModal')
      .waitForElementVisible('#closePP', 1000)
      .pause(2000)
      .click('#closePP')
      .waitForElementNotVisible('#ppModal', 3000)
      .assert.hidden('#ppModal')
  },
  'Test Valid Form Now' : browser =>{
    browser
      // Step 1
      .waitForElementVisible('#step1wrapper', 2000)
      .assert.elementPresent(".col-sm-6 div #last_name")
      .setValue('#first_name', 'Felipe')
      .pause(2000)
      .setValue('.col-sm-6 div #last_name', 'Tadeo')
      .setValue('#email', 'contact@felipetadeo.com')
      .setValue('#phone', '2096750948')
      .setValue('#zip', '93635')
      .click('#terms')
      .pause('1000')
      .getAttribute('button[type=submit]', 'disabled', function(result) {this.assert.equal(result.value, null);})
      .click('button[type=submit]')
      // Test Step 2
      .waitForElementVisible("#step2", 3000)
      .setValue('#dob', '05031987')
      .pause('500')
      .setValue('#height', '509')
      .click('#optionsRadios1')
      .pause('1000')
      .getAttribute('button[type=submit]', 'disabled', function(result) {this.assert.equal(result.value, null);})
      .click('button[type=submit]')
      // Test Step3
      .waitForElementVisible("#step3", 3000)
      .pause('1000')
      .click('#tobacco_yes')
      .pause('1000')
      .getAttribute('button[type=submit]', 'disabled', function(result) {this.assert.equal(result.value, null);})
      .click('button[type=submit]')
      // Test Step4
      .waitForElementVisible("#step4", 3000)
      .pause('1000')
      .click('#optionsRadios1')
      .click('#hypermed1')
      .click('#group11')
      .click('#diabetes1')
      .pause('1000')
      .getAttribute('button[type=submit]', 'disabled', function(result) {this.assert.equal(result.value, null);})
      .click('button[type=submit]')

      // Test Step 5
      .waitForElementVisible("#step5", 3000)
      .pause('1000')
      .click('#diseases1')
      .click('#aidsHiv2')
      .click('#addiction2')
      .pause('1000')
      .getAttribute('button[type=submit]', 'disabled', function(result) {this.assert.equal(result.value, null);})
      .click('button[type=submit]')

      // Test Step 6
      .waitForElementVisible("#step6", 3000)
      .pause('1000')
      .click('#criminalCharges2')
      .click('#disabilityBenefits2')
      .pause('1000')
      .getAttribute('button[type=submit]', 'disabled', function(result) {this.assert.equal(result.value, null);})
      .click('button[type=submit]')
      // Test Step 7
      .waitForElementVisible("#step7", 3000)
      .pause('1000')
      .click('#contactDay') // Test that datepicker works
      .waitForElementVisible('#ui-datepicker-div', 3000)
      .pause('1000')
      .click('#contactDay')
      .pause('1000')
      .useXpath()
      .click('(//div[@id="ui-datepicker-div"]/table[@class="ui-datepicker-calendar"]/tbody/tr/td/a)[1]')
      .useCss()
      .waitForElementNotVisible('#ui-datepicker-div', 3000)
      .pause('1000')
      .setValue('#contactTime', '8am-12pm')
      .click("#consent")
      .pause('1000')
      .getAttribute('button[type=submit]', 'disabled', function(result) {this.assert.equal(result.value, null);})
      .pause('1000')
      .submitForm("#final-step")

      // Testing searching
      .waitForElementVisible("#searching", 3000)
      .waitForElementVisible("#looking_for_agent", 3000)
      .assert.visible("#looking_for_agent")
      .waitForElementVisible("#agent_wrapper", 10000)
      .waitForElementVisible("#jose_wrapper", 3000)
  },
  after: browser=> browser.end()
};
